# GitLab Runner generator directly from GitLab, to Clever Cloud

- This project create and deploy **GitLab Runners** on **[Clever Cloud](https://www.clever-cloud.com/)** from a GitLab instance (self hosted or GitLab.com)
- The created GitLab Runner comes with NodeJS (but you can install what you want, see `Dockerfile`)

> The script `go.sh` is needed to register the GitLab Runner and start it on Clever Cloud.

## How to use it?

### First

- Fork or clone this project
- Push the fork or the clone to GitLab.com or your own GitLab instance

### Add secret variables

Go to **Settings > CI/CD > Variables** and add this variables:

- `APP_NAME`, the name of your runner
- `ORGANIZATION_NAME`, the name of the organization (on Clever Cloud) where you are deploying the runner
- `GITLAB_INSTANCE`, the name of the GitLab instance using the runner
- `REGION`, the region of the deployment (eg: `par` for Paris)
- `CLEVER_SECRET`, this value is used for the authentication on Clever Cloud
- `CLEVER_TOKEN`, this value is used for the authentication on Clever Cloud
- `PERSONAL_ACCESS_TOKEN`, this is your access token on the GitLab instance (or on GitLab.com) *it is used from the `go.sh` script to get the id of the runner with the GitLab API*
- `REGISTRATION_TOKEN`, this is the CI/CD token of the project using the runner (or the admin token for shared runners)

> :wave: *Remark*: Since GitLab 10.8 you can override these values by specifying variables when using manual pipelines.

### Generate a runner

- Go to the **CI/CD > Pipelines** section
- Click on the button **Run Pipeline**
- If needed, override the values
- Click on the button **Create Pipeline**
- You'll get 2 manual jobs:
  - **create** from the `🚀create_runner` stage
  - **destroy** from the `🗑destroy_runner` stage

Click on **create** and in some moments you'll obtain a GitLab Runner. (of course **destroy** is to delete the runner from Clever Cloud)





