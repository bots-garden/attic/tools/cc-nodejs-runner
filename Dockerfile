# Runner
FROM ubuntu:16.04

COPY go.sh go.sh
RUN chmod +x go.sh

# Install gitlab-runner and nodejs
RUN apt-get update && \
    apt-get install -y curl && \
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash  && \
    apt-get install -y gitlab-runner && \
    echo "👋 🦊 Runner is installed" && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash  && \
    apt-get -y install nodejs && \
    echo "👋 🦊 NodeJS is installed" 

EXPOSE 8080

CMD [ "/go.sh" ]
